package aerdocesgourmet.com.br.aerdocesgourmet;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by renat on 25/11/2017.
 */

public class PedidosDB extends SQLiteOpenHelper {



    public PedidosDB(Context context) {
        super(context, "pedidos.sqlite", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        sqLiteDatabase.execSQL("create table if not exists pedidos(" +
                "_id integer primary key autoincrement," +
                "nome text, sobrenome text, email text, endereco text, descricao text, datahora text );");

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    public long inclui(Pedidos pedido) {

        SQLiteDatabase db = getWritableDatabase();
        try {
            ContentValues dados = new ContentValues();
            dados.put("nome", pedido.getNome());
            dados.put("sobrenome", pedido.getSobrenome());
            dados.put("email", pedido.getEmail());
            dados.put("endereco", pedido.getEndereco());
            dados.put("descricao", pedido.getDescricao());
            dados.put("datahora", pedido.getDatahora());


            long id = db.insert("pedidos", "", dados);

            return id;
        } finally {
            db.close();
        }

    }

    public Pedidos busca(long id) {
        SQLiteDatabase db = getReadableDatabase();

        try {
            Cursor c = db.query("pedidos", null, "_id=?",
                    new String[]{String.valueOf(id)},
                    null, null, null);

            if (c.getCount() > 0) {
                c.moveToFirst();
                String nome = c.getString(1);
                String sobrenome = c.getString(2);
                String email = c.getString(3);
                String endereco = c.getString(4);
                String descricao = c.getString(5);
                String datahora = c.getString(6);


                return new Pedidos((int) id, nome, sobrenome, email, endereco, descricao, datahora);
            } else {
                return new Pedidos(0, "", "", "", "", "", "");
            }

        } finally {
            db.close();
        }

    }

    public long exclui(long id) {

        SQLiteDatabase db = getWritableDatabase();
        try {

            return db.delete("pedidos", "_id=?", new String[]{String.valueOf(id)});

        } finally {
            db.close();
        }

    }







}
