package aerdocesgourmet.com.br.aerdocesgourmet;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by renat on 19/11/2017.
 */

public interface ProdutosService {

    @GET("lista_produtos.php")
    Call<List<Produtos>> getProdutos();

}
