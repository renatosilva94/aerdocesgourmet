package aerdocesgourmet.com.br.aerdocesgourmet;

import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.NumberFormat;
import java.util.Locale;

/**
 * Created by renat on 22/11/2017.
 */

public class InformacoesActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView txtNomeDoce;
    private TextView txtValor;
    private TextView txtUnidadeComercial;
    private ImageView imgDoce;
    private String produto_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_informacoes);

        imgDoce = (ImageView) findViewById(R.id.imgDoce);
        txtNomeDoce = (TextView) findViewById(R.id.txtNomeDoce);
        txtValor = (TextView) findViewById(R.id.txtValor);
        txtUnidadeComercial = (TextView) findViewById(R.id.txtUnidadeComercial);


        Intent it = getIntent();
        String id = it.getStringExtra("id");
        String nome_doce = it.getStringExtra("nome_doce");
        double valor = it.getDoubleExtra("valor", -1);
        String unidade_comercial = it.getStringExtra("unidade_comercial");



        produto_id = it.getStringExtra("id");


        Picasso.with(this).load("http://aerdocesgourmet.com.br/webservice_android/fotos_produtos/"+id+".jpg").into(imgDoce);

        NumberFormat nfBr = NumberFormat.getCurrencyInstance(new Locale("pt", "BR"));


        txtNomeDoce.setText("Nome do Doce: " + nome_doce);
        txtValor.setText("Valor: " + nfBr.format(valor));
        txtUnidadeComercial.setText("Unidade Comercial: " + unidade_comercial);


    }


    @Override
    public void onClick(View view) {

    }
}
