package aerdocesgourmet.com.br.aerdocesgourmet;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

/**
 * Created by renat on 19/11/2017.
 */

public class ProdutosAdapter extends BaseAdapter {

    private Context ctx;
    private List<Produtos> produtos;
    private String pathFotos;
    private ImageView imgDoce;
    private TextView txtNomeDoce;
    private TextView txtValor;


    public ProdutosAdapter(Context ctx, List<Produtos> produtos, String pathFotos) {
        this.ctx = ctx;
        this.produtos = produtos;
        this.pathFotos = pathFotos;
    }

    @Override
    public int getCount() {
        return produtos.size();
    }

    @Override
    public Object getItem(int i) {
        return produtos.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        Produtos produto = produtos.get(i);
        View linha = LayoutInflater.from(ctx).inflate(R.layout.item_doces, null);

        imgDoce = (ImageView) linha.findViewById(R.id.imgDoce);
        txtNomeDoce = (TextView) linha.findViewById(R.id.txtNomeDoce);
        txtValor = (TextView) linha.findViewById(R.id.txtValor);


        Picasso.with(ctx).load(pathFotos+produto.getId()+".jpg").into(imgDoce);
        txtNomeDoce.setText("Nome do Produto: " + produto.getNome_doce());

        NumberFormat nfbr = NumberFormat.getCurrencyInstance(new Locale("pt", "BR"));
        txtValor.setText("Preço: " + nfbr.format(produto.getValor()));


        return linha;





    }
}
