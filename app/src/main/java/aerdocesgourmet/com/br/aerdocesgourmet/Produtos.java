package aerdocesgourmet.com.br.aerdocesgourmet;

/**
 * Created by renat on 19/11/2017.
 */

public class Produtos {

    private int id;
    private String nome_doce;
    private double valor;
    private String unidade_comercial;

    public Produtos(int id, String nome_doce, double valor, String unidade_comercial) {
        this.id = id;
        this.nome_doce = nome_doce;
        this.valor = valor;
        this.unidade_comercial = unidade_comercial;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome_doce() {
        return nome_doce;
    }

    public void setNome_doce(String nome_doce) {
        this.nome_doce = nome_doce;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public String getUnidade_comercial() {
        return unidade_comercial;
    }

    public void setUnidade_comercial(String unidade_comercial) {
        this.unidade_comercial = unidade_comercial;
    }

    }

