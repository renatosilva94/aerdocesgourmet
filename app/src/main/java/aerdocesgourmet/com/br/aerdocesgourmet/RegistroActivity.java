package aerdocesgourmet.com.br.aerdocesgourmet;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

/**
 * Created by renat on 14/11/2017.
 */

public class RegistroActivity extends AppCompatActivity {

    private EditText txtNome;
    private EditText txtSobrenome;
    private EditText txtEmail;
    private EditText txtSenha;
    private EditText txtConfirmaSenha;
    private Button btnRegistrar;
    private Button btnVoltar;

    @Override
    protected void onCreate(Bundle savedInstanceState){

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        txtNome = (EditText) findViewById(R.id.txtNome);
        txtSobrenome = (EditText) findViewById(R.id.txtSobrenome);
        txtEmail = (EditText) findViewById(R.id.txtEmail);
        txtSenha = (EditText) findViewById(R.id.txtSenha);
        txtConfirmaSenha = (EditText) findViewById(R.id.txtConfirmaSenha);
        btnRegistrar = (Button) findViewById(R.id.btnRegistrar);
        btnVoltar = (Button) findViewById(R.id.btnVoltar);


    }

    public void btnRegistrar(View view) {



    }

    public void btnVoltar(View view) {

        Intent it = new Intent(this, LoginActivity.class);
        startActivity(it);

    }



}
