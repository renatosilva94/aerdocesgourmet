package aerdocesgourmet.com.br.aerdocesgourmet;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by renat on 25/11/2017.
 */

public class ListaPedidosActivity extends AppCompatActivity {

    private ListView lvPedidos;
    private EditText edtId;
    private EditText edtDescricao;
    private EditText edtEndereco;
    private EditText edtDataHora;
    private EditText edtNome;
    private EditText edtSobrenome;
    private EditText edtEmail;


    private Button btnExcluir;
    private Button btnEnviarPedido;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listapedidos);


        edtNome = (EditText) findViewById(R.id.edtNome);
        edtSobrenome = (EditText) findViewById(R.id.edtSobrenome);
        edtEmail = (EditText) findViewById(R.id.edtEmail);
        edtDescricao = (EditText) findViewById(R.id.edtDescricao);
        edtEndereco = (EditText) findViewById(R.id.edtEndereco);
        edtDataHora = (EditText) findViewById(R.id.edtDataHora);
        edtId = (EditText) findViewById(R.id.edtId);
        btnEnviarPedido = (Button) findViewById(R.id.btnEnviarPedido);
        btnExcluir = (Button) findViewById(R.id.btnExcluir);


    }

    public void buscarPedidos(View view) {


        String strId = edtId.getText().toString();

        if (strId.trim().isEmpty()) {

            Toast.makeText(this, "Informe o código a ser pesquisado", Toast.LENGTH_LONG).show();
            edtId.requestFocus();
            return;

        }

        long id = Integer.parseInt(strId);

        PedidosDB pedidosDB = new PedidosDB(this);

        Pedidos pedidos = pedidosDB.busca(id);

        if (pedidos.getId() > 0) {
            edtNome.setText(pedidos.getNome());
            edtSobrenome.setText(pedidos.getSobrenome());
            edtEmail.setText(pedidos.getEndereco());
            edtDescricao.setText(pedidos.getDescricao());
            edtEndereco.setText(pedidos.getEndereco());
            edtDataHora.setText(pedidos.getDatahora());


            btnExcluir.setVisibility(View.VISIBLE);
            btnEnviarPedido.setVisibility(View.VISIBLE);


        } else {
            Toast.makeText(this, "Erro... Código não encontrado", Toast.LENGTH_LONG).show();

        }


    }





    public void excluirPedido(View view) {

        String strId = edtId.getText().toString();


        int id = Integer.parseInt(strId);

        PedidosDB pedidosDB = new PedidosDB(this);

        if(pedidosDB.exclui(id) >0){
            Toast.makeText(this, "Pedido Excluido", Toast.LENGTH_LONG).show();

            limparCampos();

        }else{
            Toast.makeText(this, "Erro... Não Foi Possivel Excluir O Pedido", Toast.LENGTH_LONG).show();

            limparCampos();
        }

    }

    public void enviarPedido(View view) {


        String nome = edtNome.getText().toString();
        String sobrenome = edtSobrenome.getText().toString();
        String email = edtEmail.getText().toString();
        String endereco = edtEndereco.getText().toString();
        String datahora = edtDataHora.getText().toString();
        String descricao = edtDescricao.getText().toString();


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://10.0.2.2/ws_aerdocesgourmet/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        PedidosService service = retrofit.create(PedidosService.class);
        Call<Pedidos> callPedidos = service.enviarPedido(nome, sobrenome, email, endereco, datahora, descricao);

        callPedidos.enqueue(new Callback<Pedidos>() {
            @Override
            public void onResponse(Call<Pedidos> call, Response<Pedidos> response) {
                if(response.isSuccessful()){
                    Toast.makeText(getApplicationContext(), "Seu Pedido Foi Realizado Com Sucesso!", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<Pedidos> call, Throwable t) {

            }
        });



    }

    private void limparCampos(){

        edtId.setText("");
        edtDescricao.setText("");
        edtEndereco.setText("");
        edtDataHora.setText("");
        edtNome.setText("");
        edtSobrenome.setText("");
        edtEmail.setText("");

        btnEnviarPedido.setVisibility(View.VISIBLE);
        btnExcluir.setVisibility(View.VISIBLE);

        edtId.requestFocus();

    }



}

