package aerdocesgourmet.com.br.aerdocesgourmet;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

public class NoticiasAdapter extends BaseAdapter {

    private Context ctx;
    private List<Noticias> noticias;
    private TextView txtTitulo;
    private TextView txtTexto;
    private TextView  txtData;



    public NoticiasAdapter(Context ctx, List<Noticias> noticias, String s) {
        this.ctx = ctx;
        this.noticias = noticias;

    }

    @Override
    public int getCount() {
        return noticias.size();
    }

    @Override
    public Object getItem(int i) {
        return noticias.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        Noticias noticia = noticias.get(i);
        View linha = LayoutInflater.from(ctx).inflate(R.layout.item_noticias, null);

        txtTitulo = (TextView) linha.findViewById(R.id.txtTitulo);
        txtData = (TextView) linha.findViewById(R.id.txtData);

        txtTitulo.setText("Titulo: " + noticia.getTitulo());
        txtData.setText("Data: " + noticia.getData());

        return linha;


    }
}
