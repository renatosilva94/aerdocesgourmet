package aerdocesgourmet.com.br.aerdocesgourmet;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import android.os.Bundle;
import android.widget.TextView;

import org.w3c.dom.Text;

/**
 * Created by renat on 19/11/2017.
 */

public class SobreActivity extends AppCompatActivity {

    private TextView txtFundadores;
    private TextView txtAnoFundacao;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sobre);




    }

    public void btnFacebook(View view){
        Intent it = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.facebook.com/AeRDocesGourmet"));
        startActivity(it);
    }

    public void btnInstagram(View view){
        Intent it = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.instagram.com/AeRDocesGourmet"));
        startActivity(it);
    }

}
