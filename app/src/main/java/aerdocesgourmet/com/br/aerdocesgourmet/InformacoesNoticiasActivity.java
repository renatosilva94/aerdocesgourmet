package aerdocesgourmet.com.br.aerdocesgourmet;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.NumberFormat;
import java.util.Locale;

public class InformacoesNoticiasActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView txtTitulo;
    private TextView txtTexto;
    private TextView txtData;
    private String noticia_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_informacoes_noticias);


        txtTitulo = (TextView) findViewById(R.id.txtTitulo);
        txtTexto = (TextView) findViewById(R.id.txtTexto);
        txtData = (TextView) findViewById(R.id.txtData);


        Intent it = getIntent();
        String id = it.getStringExtra("id");
        String titulo = it.getStringExtra("titulo");
        String texto = it.getStringExtra("texto");
        String data = it.getStringExtra("data");




        noticia_id = it.getStringExtra("id");


       // Picasso.with(this).load("http://aerdocesgourmet.com.br/webservice_android/fotos_produtos/"+id+".jpg").into(imgDoce);

        // NumberFormat nfBr = NumberFormat.getCurrencyInstance(new Locale("pt", "BR"));


        txtTitulo.setText("Titulo: " + titulo);
        txtTexto.setText("Texto: " + texto);
        txtData.setText("Data: " + data);


    }


    @Override
    public void onClick(View view) {

    }
}
