package aerdocesgourmet.com.br.aerdocesgourmet;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class NoticiasActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    private ListView lvNoticias;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_noticias);


        lvNoticias = (ListView) findViewById(R.id.lvNoticias);

        lvNoticias.setOnItemClickListener(this);


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://aerdocesgourmet.com.br/webservice_android/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        NoticiasService service = retrofit.create(NoticiasService.class);
        Call<List<Noticias>> noticias = service.getNoticias();

        noticias.enqueue(new Callback<List<Noticias>>() {
            @Override
            public void onResponse(Call<List<Noticias>> call, Response<List<Noticias>> response) {
                if(response.isSuccessful()){
                    List<Noticias> listaNoticias = response.body();
                    NoticiasAdapter adapter = new NoticiasAdapter(getApplicationContext(),
                            listaNoticias, "http://aerdocesgourmet.com.br/webservice_android/");

                    lvNoticias.setAdapter(adapter);

                }
            }

            @Override
            public void onFailure(Call<List<Noticias>> call, Throwable t) {

            }
        });

    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

        Noticias noticia = (Noticias) adapterView.getItemAtPosition(i);

        Intent it = new Intent(this, InformacoesNoticiasActivity.class);

        it.putExtra("id", "" + noticia.getId());
        it.putExtra("titulo", noticia.getTitulo());
        it.putExtra("texto", noticia.getTexto());
        it.putExtra("data", noticia.getData());


        startActivity(it);


    }
}
