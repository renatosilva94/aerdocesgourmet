package aerdocesgourmet.com.br.aerdocesgourmet;


import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
/**
 * Created by renat on 14/11/2017.
 */

public interface UsuariosService {

    @FormUrlEncoded
    @POST("cadastro_usuario.php")
    Call<Usuarios> cadastraUsuario(@Field("nome") String nome,
                                   @Field("sobrenome") String sobrenome,
                                   @Field("email") String email,
                                   @Field("senha") String senha,
                                   @Field("tipoUsuario") int tipoUsuario);


}
