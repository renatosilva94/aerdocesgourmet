package aerdocesgourmet.com.br.aerdocesgourmet;

/**
 * Created by renat on 20/11/2017.
 */

public class Pedidos {

    private int id;
    private String nome;
    private String sobrenome;
    private String email;
    private String descricao;
    private String endereco;
    private String datahora;

    public Pedidos(int id,String nome, String sobrenome, String email, String descricao, String endereco, String datahora) {
        this.id = id;
        this.nome = nome;
        this.sobrenome = sobrenome;
        this.email = email;
        this.descricao = descricao;
        this.endereco = endereco;
        this.datahora = datahora;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSobrenome() {
        return sobrenome;
    }

    public void setSobrenome(String sobrenome) {
        this.sobrenome = sobrenome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getDatahora() {
        return datahora;
    }

    public void setDatahora(String datahora) {
        this.datahora = datahora;
    }
}
