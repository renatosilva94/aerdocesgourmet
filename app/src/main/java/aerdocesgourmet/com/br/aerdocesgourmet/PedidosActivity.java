package aerdocesgourmet.com.br.aerdocesgourmet;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by renat on 20/11/2017.
 */

public class PedidosActivity extends AppCompatActivity {

    private EditText edtEmail;
    private EditText edtNome;
    private EditText edtSobrenome;
    private EditText edtEndereco;
    private EditText edtDescricao;
    private EditText edtDataHora;
    private Button btnFazerPedido;
    private Button btnSalvarPedido;
    private Button btnCancelar;
    private Button btnMeusPedidos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pedidos);

        edtEmail = (EditText) findViewById(R.id.edtEmail);
        edtNome = (EditText) findViewById(R.id.edtNome);
        edtSobrenome = (EditText) findViewById(R.id.edtSobrenome);
        edtEndereco = (EditText) findViewById(R.id.edtEndereco);
        edtDataHora = (EditText) findViewById(R.id.edtDataHora);
        edtDescricao = (EditText) findViewById(R.id.edtDescricao);
        btnFazerPedido = (Button) findViewById(R.id.btnFazerPedido);
        btnSalvarPedido = (Button) findViewById(R.id.btnSalvarPedido);
        btnCancelar = (Button) findViewById(R.id.btnCancelar);
        btnMeusPedidos = (Button) findViewById(R.id.btnMeusPedidos);

    }


    public void btnFazerPedido(View view){

        String nome = edtNome.getText().toString();
        String sobrenome = edtSobrenome.getText().toString();
        String email = edtEmail.getText().toString();
        String endereco = edtEndereco.getText().toString();
        String datahora = edtDataHora.getText().toString();
        String descricao = edtDescricao.getText().toString();

        if(nome.trim().isEmpty() || sobrenome.trim().isEmpty() || email.trim().isEmpty() || endereco.trim().isEmpty() || datahora.trim().isEmpty() || descricao.trim().isEmpty()){
            Toast.makeText(this, "Preencha todos os campos", Toast.LENGTH_LONG).show();
            edtNome.requestFocus();
            return;
        }

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://10.0.2.2/ws_aerdocesgourmet/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        PedidosService service = retrofit.create(PedidosService.class);
        Call<Pedidos> callPedidos = service.enviarPedido(nome, sobrenome, email, endereco, datahora, descricao);

        callPedidos.enqueue(new Callback<Pedidos>() {
            @Override
            public void onResponse(Call<Pedidos> call, Response<Pedidos> response) {
                if(response.isSuccessful()){
                    Toast.makeText(getApplicationContext(), "Seu Pedido Foi Realizado Com Sucesso!", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<Pedidos> call, Throwable t) {

            }
        });
    }



    public void btnSalvarPedido(View view){

        String nome = edtNome.getText().toString();
        String sobrenome = edtSobrenome.getText().toString();
        String email = edtEmail.getText().toString();
        String endereco = edtEndereco.getText().toString();
        String descricao = edtDescricao.getText().toString();
        String datahora = edtDataHora.getText().toString();


        if(nome.trim().isEmpty() || sobrenome.trim().isEmpty() || email.toString().trim().isEmpty() || endereco.toString().trim().isEmpty() || descricao.toString().trim().isEmpty() || datahora.toString().trim().isEmpty()){
            Toast.makeText(this,"Preencha todos os campos", Toast.LENGTH_LONG).show();
            edtNome.requestFocus();
            return;
        }



        PedidosDB pedidosDB = new PedidosDB(this);

        long id = pedidosDB.inclui(new Pedidos(0, nome, sobrenome, email, endereco, descricao, datahora));

        if(id > 0) {

            Toast.makeText(this, "Pedido Salvo com ID: " + id, Toast.LENGTH_LONG).show();

        }else{

            Toast.makeText(this, "Erro ao Salvar o Seu Pedido", Toast.LENGTH_LONG).show();


        }



        edtNome.setText("");
        edtSobrenome.setText("");
        edtEmail.setText("");
        edtEndereco.setText("");
        edtDescricao.setText("");
        edtDataHora.setText("");


        edtNome.requestFocus();

    }


    public void btnMeusPedidos(View view){

        Intent it = new Intent(this, ListaPedidosActivity.class);
        startActivity(it);

    }


    public void btnCancelar(View view){
        Intent it = new Intent(this, MenuActivity.class);
        startActivity(it);
    }
}
