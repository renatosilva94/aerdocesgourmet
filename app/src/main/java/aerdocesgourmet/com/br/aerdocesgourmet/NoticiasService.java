package aerdocesgourmet.com.br.aerdocesgourmet;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface NoticiasService {

    @GET("lista_noticias.php")
    Call<List<Noticias>> getNoticias();


}
