package aerdocesgourmet.com.br.aerdocesgourmet;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by renat on 19/11/2017.
 */

public class ProdutosActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    private ListView lvDoces;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_produtos);


        lvDoces = (ListView) findViewById(R.id.lvDoces);

        lvDoces.setOnItemClickListener(this);


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://aerdocesgourmet.com.br/webservice_android/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        ProdutosService service = retrofit.create(ProdutosService.class);
        Call<List<Produtos>> produtos = service.getProdutos();

        produtos.enqueue(new Callback<List<Produtos>>() {
            @Override
            public void onResponse(Call<List<Produtos>> call, Response<List<Produtos>> response) {
                if(response.isSuccessful()){
                    List<Produtos> listaProdutos = response.body();
                    ProdutosAdapter adapter = new ProdutosAdapter(getApplicationContext(),
                            listaProdutos, "http://aerdocesgourmet.com.br/webservice_android/fotos_produtos/");

                    lvDoces.setAdapter(adapter);

                }
            }

            @Override
            public void onFailure(Call<List<Produtos>> call, Throwable t) {

            }
        });

    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

        Produtos produto = (Produtos) adapterView.getItemAtPosition(i);

        Intent it = new Intent(this, InformacoesActivity.class);

        it.putExtra("id", "" + produto.getId());
        it.putExtra("nome_doce", produto.getNome_doce());
        it.putExtra("valor", produto.getValor());
        it.putExtra("unidade_comercial", produto.getUnidade_comercial());


        startActivity(it);


    }
}
