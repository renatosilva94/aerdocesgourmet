package aerdocesgourmet.com.br.aerdocesgourmet;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by renat on 20/11/2017.
 */

public interface PedidosService {

    @FormUrlEncoded
    @POST("enviar_pedido.php")
    Call<Pedidos> enviarPedido(@Field("nome") String nome,
                                @Field("sobrenome") String sobrenome,
                                @Field("email") String email,
                                @Field("endereco") String endereco,
                                @Field("datahora") String datahora,
                                @Field("descricao") String descricao
    );
}
