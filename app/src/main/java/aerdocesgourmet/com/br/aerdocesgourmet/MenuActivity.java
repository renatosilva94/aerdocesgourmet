package aerdocesgourmet.com.br.aerdocesgourmet;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

/**
 * Created by renat on 14/11/2017.
 */

public class MenuActivity extends AppCompatActivity{

    private Button btnProdutos;
    private Button btnNoticias;
    private Button btnSobre;
    private Button btnPedidos;
    private Button btnSair;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        btnProdutos = (Button) findViewById(R.id.btnProdutos);
        btnNoticias = (Button) findViewById(R.id.btnNoticias);
        btnSobre = (Button) findViewById(R.id.btnSobre);
        btnPedidos = (Button) findViewById(R.id.btnPedidos);
        btnSair = (Button) findViewById(R.id.btnSair);


    }

    public void btnProdutos(View view) {

        Intent it = new Intent(this, ProdutosActivity.class);
        startActivity(it);

    }

    public void btnNoticias(View view) {

        Intent it = new Intent(this, NoticiasActivity.class);
        startActivity(it);

    }

    public void btnSobre(View view){

        Intent it = new Intent(this, SobreActivity.class);
        startActivity(it);

    }

    public void btnSair(View view){
        this.finish();
    }



    public void btnPedidos(View view){

        Intent it = new Intent(this, PedidosActivity.class);
        startActivity(it);

    }

}
